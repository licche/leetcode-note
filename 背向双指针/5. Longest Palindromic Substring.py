Medium
4563413FavoriteShare
Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.
Example 1:
Input: "babad"
Output: "bab"
Note: "aba" is also a valid answer.
Example 2:
Input: "cbbd"
Output: "bb"

class Solution(object):        
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        #记录最长的字符串
        index_left = index_right = -1
        longest = -1
        for i in range(len(s)):
            #由一个字母向两边找 aba
            left , right = self.check_palindromic(s, i, i)
            if right - left > longest:
                longest = right - left
                index_left, index_right = left, right
            
            #由两个字母向两边找 abba
            left , right = self.check_palindromic(s, i, i + 1)
            if right - left > longest:
                longest = right - left
                index_left, index_right = left, right
        
        return s[index_left: index_right]
            
            
    def check_palindromic(self, s, left, right):
        while left >= 0 and right < len(s) and s[left] == s[right]:
            left -= 1
            right += 1
            
        #返回的left需要+1，因为left时超范围或者不符合才停的，right同理
        return left + 1, right


class Solution(object):        
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        #记录最长的字符串
        longest = ''
        for i in range(len(s)):
            #由一个字母向两边找 aba
            left , right = self.check_palindromic(s, i, i)
            if right - left > len(longest):
                longest = s[left:right]
            
            #由两个字母向两边找 abba
            left , right = self.check_palindromic(s, i, i + 1)
            if right - left > len(longest):
                longest = s[left:right]
                
        return longest
            
            
    def check_palindromic(self, s, left, right):
        while left >= 0 and right < len(s) and s[left] == s[right]:
            left -= 1
            right += 1
            
        #返回的left需要+1，因为left时超范围或者不符合才停的，right同理
        return left + 1, right


class Solution(object):        
    def longestPalindrome(self, s):
        if not s:
            return ""
        
        n = len(s)
        dp = [[False for _ in range(n)] for _ in range(n)]
        result = ""
        
        for i in range(n - 1, -1, -1):
            for j in range(i, n):
                if s[i] == s[j] and (j - i < 3 or dp[i + 1][j - 1] == True):
                    dp[i][j] = True
                    if j - i + 1 > len(result):
                        result = s[i:j+1]
        
        return result
