Medium
194496Add to ListShare
Given a string, your task is to count how many palindromic substrings in this string.
The substrings with different start indexes or end indexes are counted as different substrings even they consist of same characters.
Example 1:
Input: "abc"
Output: 3
Explanation: Three palindromic strings: "a", "b", "c".
 
Example 2:
Input: "aaa"
Output: 6
Explanation: Six palindromic strings: "a", "a", "a", "aa", "aa", "aaa".
 
Note:
	1. The input string length won't exceed 1000.

来自 <https://leetcode.com/problems/palindromic-substrings/> 

class Solution:
    def countSubstrings(self, s: str) -> int:
        
        count = 0
        for i in range(len(s)):
        
            count += self.check_palindromic(s, i, i)            
            count += self.check_palindromic(s, i, i + 1)
                
        return count
                
    
    def check_palindromic(self, s, left, right):
        count = 0
        
        while left >= 0 and right < len(s) and s[left] == s[right]:

            left -= 1
            right += 1
            count += 1

        
        return count


class Solution:
    def countSubstrings(self, s: str) -> int:
        dp = [[0 for _ in range(len(s))] for _ in range(len(s))]
        count = 0
        for i in range(len(s)):
            for j in range(i + 1):
                if s[i] == s[j] and (i - j < 2 or dp[j + 1][i - 1] == 1):
                    dp[j][i] = 1
                count += dp[j][i]
                
        return count