Medium
45217Add to ListShare
Let call any (contiguous) subarray B (of A) a mountain if the following properties hold:
	• B.length >= 3
	• There exists some 0 < i < B.length - 1 such that B[0] < B[1] < ... B[i-1] < B[i] > B[i+1] > ... > B[B.length - 1]
(Note that B could be any subarray of A, including the entire array A.)
Given an array A of integers, return the length of the longest mountain. 
Return 0 if there is no mountain.
Example 1:
Input: [2,1,4,7,3,2,5]
Output: 5
Explanation: The largest mountain is [1,4,7,3,2] which has length 5.
Example 2:
Input: [2,2,2]
Output: 0
Explanation: There is no mountain.
Note:
	1. 0 <= A.length <= 10000
	2. 0 <= A[i] <= 10000
Follow up:
	• Can you solve it using only one pass?
	• Can you solve it in O(1) space?

来自 <https://leetcode.com/problems/longest-mountain-in-array/> 


DP:
    def longestMountain(self, A: List[int]) -> int:
        
        longest = 0
        
        up = [0 for _ in A]
        down = [0 for _ in A]
        
        #up存到该点之前时，最长连续递增的子序列多长
        for i in range(1, len(A)):
            if A[i] > A[i - 1]:
                up[i] = up[i - 1] + 1
        
        #down是从后往前找，最长连续递增的子序列多长
        for j in range(len(A) - 2, -1, -1):
            if A[j] > A[j + 1]:
                down[j] = down[j + 1] + 1
                
        #找山峰，如果是就记录它的长度
        for p in range(1, len(A) - 1):
            if A[p - 1] < A[p] > A[p + 1]:
                
                longest = max(longest, up[p] + down[p] + 1)
            

                
        return longest


双指针:
class Solution:
    def longestMountain(self, A: List[int]) -> int:
        
        longest = 0
        
        i = 0
        while i < len(A) - 1:
                    
            point = i
            
            #如果开始是递增的，再进入循环
            if point + 1 < len(A) and A[point] < A[point + 1]:
                
                #找到第一个山峰的点
                while point + 1 < len(A) and A[point + 1] > A[point]:
                    point += 1
                
                #如果后面是下降的再进入循环
                if point + 1 < len(A) and A[point + 1] < A[point]:
                    
                    #找到下降的最远点
                    while point + 1 < len(A) and A[point + 1] < A[point]:
                        point += 1
                    
                    #记录当前山峰的长度
                    longest = max(longest, point - i + 1) 
                
            #更新i的位置，要么是point所以遍历的位置，要么是i的下一位
            i = max(point, i + 1)

                
        return longest



class Solution:
    def longestMountain(self, A: List[int]) -> int:
        
        longest = 0
        for i in range(1, len(A) - 1):
            
            #如果当前值是peak再往下找
            if A[i - 1] < A[i] > A[i + 1]: 
                left = right = i
                
                #左右分别找最远
                while left - 1 >= 0 and A[left] > A[left - 1]:
                    left -= 1
                    
                while right + 1 < len(A) and A[right] > A[right + 1]:
                    right += 1
                
                #记录最远
                longest = max(longest, right - left + 1)
                
        return longest
