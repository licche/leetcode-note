Medium
1209180FavoriteShare
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.
(i.e.,  [0,1,2,4,5,6,7] might become  [4,5,6,7,0,1,2]).
Find the minimum element.
You may assume no duplicate exists in the array.
Example 1:
Input: [3,4,5,1,2] 
Output: 1
Example 2:
Input: [4,5,6,7,0,1,2]
Output: 0


class Solution:
    def findMin(self, nums: List[int]) -> int:
        start, end = 0, len(nums)-1
        #二分法三部曲：取中值，判断往哪边递归，返回目标值
        while start + 1 < end:
            mid = start + (end - start) // 2
            #只需要中值跟end比，中值大的话最小值一定在右侧
            if nums[mid] > nums[end]:
                start = mid
            else:
                end = mid
        #返回最小值，不是index
        return min(nums[start], nums[end])

