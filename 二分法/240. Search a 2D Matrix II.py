Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the following properties:
	• Integers in each row are sorted in ascending from left to right.
	• Integers in each column are sorted in ascending from top to bottom.
Example:
Consider the following matrix:
[
  [1,   4,  7, 11, 15],
  [2,   5,  8, 12, 19],
  [3,   6,  9, 16, 22],
  [10, 13, 14, 17, 24],
  [18, 21, 23, 26, 30]
]
Given target = 5, return true.
Given target = 20, return false.

分析
• 矩阵特点：每一行和每一列递增
从左下角往右上角找，有如下三种情况：
1. 元素 = target，返回true
2. 元素 < target，下一步向右走，因为右边的元素都大于当前元素，上方元素小于当前元素
3. 元素 > target，下一步向上走，因为右边的元素都大于当前元素，上方元素小于当前元素


class Solution:
    def searchMatrix(self, matrix, target):
        """
        :type matrix: List[List[int]]
        :type target: int
        :rtype: bool
        """
        if len(matrix) == 0 or len(matrix[0]) == 0:
            return False
        
        #定义行数和列数备用
        row = len(matrix)
        col = len(matrix[0])
        #从左下角开始遍历
        r = row - 1
        c = 0
        #r从下往上减，c从前往后增
        while r >= 0 and c < col:
            if matrix[r][c] == target:
                return True
            #如果该位置小于target，位置向后移一位
            elif matrix[r][c] < target:
                c += 1
            #如果该位置大于target，位置向上移一行
            else:
                r -= 1
        
        return False