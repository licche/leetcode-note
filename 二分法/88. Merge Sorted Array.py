Easy
12883061FavoriteShare
Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.
Note:
	• The number of elements initialized in nums1 and nums2 are m and n respectively.
	• You may assume that nums1 has enough space (size that is greater or equal to m + n) to hold additional elements from nums2.
Example:
Input:
nums1 = [1,2,3,0,0,0], m = 3
nums2 = [2,5,6],       n = 3
Output: [1,2,2,3,5,6]

分析
考点：从后往前合并，因为后面的空间是空的，不会覆盖原有元素。


class Solution:
    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        """
        Do not return anything, modify nums1 in-place instead.
        """
        #因为nums1后面为空，肯定要往它后面插元素
        while m > 0 and n > 0:
            #当nums1最后一位小于nums2最后一位时，把nums2最后一位放到nums1后面的空位置
            if nums1[m-1] < nums2[n-1]:
                nums1[m+n-1] = nums2[n-1]
                #下标n向前走一位
                n = n - 1
            #否则nums1大于nums2，把nums1和最后一位互换
            else:
                nums1[m+n-1], nums1[m-1] = nums1[m-1], nums1[m+n-1]
                m = m - 1
        #因为是往nums1里插，当nums2没插完的时候需要把剩下的全插到1的前面
        if m == 0 and n > 0:
            nums1[:n] = nums2[:n]
