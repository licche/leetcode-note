Easy
8811515FavoriteShare
Implement int sqrt(int x).
Compute and return the square root of x, where x is guaranteed to be a non-negative integer.
Since the return type is an integer, the decimal digits are truncated and only the integer part of the result is returned.
Example 1:
Input: 4
Output: 2
Example 2:
Input: 8
Output: 2
Explanation: The square root of 8 is 2.82842..., and since 
             the decimal part is truncated, 2 is returned.

分析
思路就是找到最后一个number k，满足条件k^2 < x，则k就是结果


class Solution:
    def mySqrt(self, x: int) -> int:
        start, end = 1, x
        while start + 1 < end:
            mid = start + (end - start) // 2
            #判断中值
            if mid * mid == x:
                return mid
            #中值太小，向右
            elif mid * mid < x:
                start = mid
            #中值太大，向左
            else:
                end = mid
        #找到start和end离x最近的一个作为结果
        if end * end <= x:
            return end
        else:
            return start