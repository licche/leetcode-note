题目
Given an array of integers sorted in ascending order, find the starting and ending position of a given target value.
Your algorithm’s runtime complexity must be in the order of O(log n).
If the target is not found in the array, return [-1, -1].
For example,
Given [5, 7, 7, 8, 8, 10] and target value 8,
return [3, 4].
分析
需要分别找到n第一次出现的位置和最后一次出现的位置，返回即可。


class Solution:
    def findPosition(self, nums, target: int) -> int:
        if len(nums) == 0:
            return -1
        res = []
        start, end = 0, len(nums) - 1

        while start + 1 < end:
            mid = start + (end - start)//2
            if nums[mid] == target:
                end = mid #搜索前面
            elif nums[mid] < target:
                start = mid
            else:
                end = mid
        if nums[start] == target:
            res.append(start)
        if nums[end] == target:
            res.append(end)

        start, end = 0, len(nums) - 1
        while start + 1 < end:
            mid = start + (end - start)//2
            if nums[mid] == target:
                start = mid#搜索后面
            elif nums[mid] < target:
                start = mid
            else:
                end = mid
        if nums[end] == target:
            res.append(end)
        if nums[start] == target:
            res.append(start)
        return res

if __name__ == '__main__':
    solution = Solution()
    a = [5, 7, 7 ,8, 8, 8, 10]

    result = solution.findPosition(a, 8)
    print(result)
