Medium
3119369FavoriteShare
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.
(i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).
You are given a target value to search. If found in the array return its index, otherwise return -1.
You may assume no duplicate exists in the array.
Your algorithm's runtime complexity must be in the order of O(log n).
Example 1:
Input: nums = [4,5,6,7,0,1,2], target = 0
Output: 4
Example 2:
Input: nums = [4,5,6,7,0,1,2], target = 3
Output: -1

来自 <https://leetcode.com/problems/search-in-rotated-sorted-array/> 

用一次二分法：
class Solution:
    def search(self, nums: List[int], target: int) -> int:
        #为了过case
        if not nums or target == None:
            return -1
        #二分法
        start, end = 0, len(nums) - 1
        while start + 1 < end:
            mid = start + (end - start) // 2
            #如果是mid >= start，则此段单调增
            if nums[mid] >= nums[start]:
                #如果target在此单调增，在此处循环，否则在后面
                if nums[start] <= target <= nums[mid]:
                    end = mid
                else:
                    start = mid
            #如果是mid < start，则在后段单调增
            else:
                if nums[mid] <= target <= nums[end]:
                    start = mid
                else:
                    end = mid
        if nums[start] == target:
            return start
        elif nums[end] == target:
            return end
        else:
            return -1
