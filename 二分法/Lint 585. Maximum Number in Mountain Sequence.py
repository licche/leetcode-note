Description
中文English
Given a mountain sequence of n integers which increase firstly and then decrease, find the mountain top.
Have you met this question in a real interview?  Yes
Problem Correction
Example
Example 1:
Input: nums = [1, 2, 4, 8, 6, 3] 
Output: 8
Example 2:
Input: nums = [10, 9, 8, 7], 
Output: 10

来自 <https://www.lintcode.com/problem/maximum-number-in-mountain-sequence/description?_from=ladder&&fromId=1> 
