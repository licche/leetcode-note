Description
中文
English
写出一个高效的算法来搜索m×n矩阵中的值，返回这个值出现的次数。
这个矩阵具有以下特性：
• 每行中的整数从左到右是排序的。
• 每一列的整数从上到下是排序的。
• 在每一行或每一列中没有重复的整数。
Have you met this question in a real interview?  Yes
Problem Correction
Example
例1:
输入:
[[3,4]]
target=3
输出:1
例2:
输入:
    [
      [1, 3, 5, 7],
      [2, 4, 7, 8],
      [3, 5, 9, 10]
    ]
    target = 3
输出:2
Challenge
要求O(m+n) 时间复杂度和O(1) 额外空间

来自 <https://www.lintcode.com/problem/search-a-2d-matrix-ii/description> 
