Hard
4977720FavoriteShare
There are two sorted arrays nums1 and nums2 of size m and n respectively.
Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
You may assume nums1 and nums2 cannot be both empty.
Example 1:
nums1 = [1, 3]
nums2 = [2]
The median is 2.0
Example 2:
nums1 = [1, 2]
nums2 = [3, 4]
The median is (2 + 3)/2 = 2.5

分析
令nums1.length = m;nums2.length = n;总长度m+n。先确定中位数m是多少:
	1. (m+n)%2 == 0:
m=(m+n)/2,(m+n)/2+1
	2. (m+n)%2 == 1:
m=(m+n)/2
问题转化为找两个有序数组的第K大的问题，如果用merge的方法获得merge之后的排序数组需要O(m+n)的时间复杂度，题目要求使用O(log(m+n))的时间复杂度，所以需要使用二分法：
如果A[k/2] <= B[k/2]：A的前k/2个数一定都在A、B合并后的前K个数中，去掉A的前k/2个元素，继续寻找A和B的第m-k/2个元素。
如果A[k/2] > B[k/2]：B的前k/2个数一定都在A、B合并后的前K个数中，去掉B的前k/2个元素，继续寻找A和B的第m-k/2个元素。
如果A[k/2]越界，A中剩余元素不足k/2个，则B中前k/2个元素一定在前K个中，去掉B的前k/2个元素，继续寻找A和B的第m-k/2个元素。
如果B[k/2]越界，B中剩余元素不足k/2个，则A中前k/2个元素一定在前K个中，去掉A的前k/2个元素，继续寻找A和B的第m-k/2个元素。
边界条件判断：
	1. nums1中没有元素了,直接返回nums2中的第k个
	2. nums2中没有元素了,直接返回nums1中的第k个
	3. k == 1，递归出口，直接返回min(nums1[start1],nums2[start2])
