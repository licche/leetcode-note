Medium
795167FavoriteShare
Given a sorted array, two integers k and x, find the k closest elements to x in the array. The result should also be sorted in ascending order. If there is a tie, the smaller elements are always preferred.
Example 1:
Input: [1,2,3,4,5], k=4, x=3
Output: [1,2,3,4]
Example 2:
Input: [1,2,3,4,5], k=4, x=-1
Output: [1,2,3,4]
Note:
	1. The value k is positive and will always be smaller than the length of the sorted array.
	2. Length of the given array is positive and will not exceed 104
	3. Absolute value of elements in the array and x will not exceed 104

UPDATE (2017/9/19):
The arr parameter had been changed to an array of integers (instead of a list of integers). Please reload the code definition to get the latest changes.

思路：
先用二分法找到数组中>=x的第一个数字，然后向左向右寻找，两个指针向左右移动，直至k次结束，此时将两个指针中间的元素加入结果集即是最终答案。


class Solution:
    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:
        #二分法找到第一个大于x的数
        start, end = 0, len(arr) - 1
        while start + 1 < end:
            mid = start + (end - start) // 2
            #中值大于等于x时候，end挪过来，为了使得end作为第一个大于x的数
            if arr[mid] >= x:
                end = mid
            else:
                start = mid
        
        left = end - 1
        right = end
        nums = 0
        res = []
        #当计数少于k时循环
        while nums < k:
            #初始化左右数据为无穷大，防止左右一边循环完的情况
            ldata = float('inf')
            rdata = float('inf')
            #边界条件就是arr的范围
            if left >= 0:
                #必须取绝对值
                ldata = abs(arr[left] - x)
            if right < len(arr):
                rdata = abs(arr[right] - x)
            #比较两边差值的大小，左边的小于等于，往左走，优先选左边
            if ldata <= rdata:
                left -= 1
            else: 
                right += 1
            nums += 1
        #打印范围是左加1，到右减1
        for i in range(left+1, right):
            res.append(arr[i])
        return res


class Solution:
    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:
        #二分法找到第一个大于x的数
        start, end = 0, len(arr) - 1
        while start + 1 < end:
            mid = start + (end - start) // 2
            #中值大于等于x时候，end挪过来，为了使得end作为第一个大于x的数
            if arr[mid] >= x:
                end = mid
            else:
                start = mid       

        res = []
        #当长度少于k时循环
        while len(res) < k:
            #太小，压入end的值
            if start < 0:
                res.append(arr[end])
                end += 1
            #太大，压入start的值
            elif end > len(arr) - 1:
                res.insert(0,arr[start])
                start -= 1
            #在中间的话需要比较start和end哪个离x近
            else:
                ldata = x - arr[start]
                rdata = arr[end] - x
                if ldata <= rdata:
                    res.insert(0, arr[start])
                    start -= 1
                else:
                    res.append(arr[end])
                    end += 1
        return res



