Description
中文English
Calculate the an % b where a, b and n are all 32bit non-negative integers.
Have you met this question in a real interview?  Yes
Problem Correction
Example
For 231 % 3 = 2
For 1001000 % 1000 = 0
Challenge
O(logn)
Related Problems

来自 <https://www.lintcode.com/problem/fast-power/description?_from=ladder&&fromId=1> 

思路：	
	主要利用公式：(a * b) % p = ((a % p) * (b % p)) % p 
	运用recursion的思路
