A peak element is an element that is greater than its neighbors.
Given an input array nums, where nums[i] ≠ nums[i+1], find a peak element and return its index.
The array may contain multiple peaks, in that case return the index to any one of the peaks is fine.
You may imagine that nums[-1] = nums[n] = -∞.
Example 1:
Input: nums = [1,2,3,1]
Output: 2
Explanation: 3 is a peak element and your function should return the index number 2.
Example 2:
Input: nums = [1,2,1,3,5,6,4]
Output: 1 or 5 
Explanation: Your function can return either index number 1 where the peak element is 2, 
             or index number 5 where the peak element is 6.
Note:
Your solution should be in logarithmic complexity.

分析
给定一个数组，返回peak的元素（左边右边都比自己小），如果有多个返回任意一个即可，要求时间复杂度为O(logn)
遇到O(logn)要考虑到二分法：
二分法选取mid元素，其跟左右元素的关系有如下四中情况：
	1. 两边元素都比mid小，mid就是peak,，返回
	2. 两边元素都比mid大，左右都有可能有peak，任选一边二分
	3. 左边小又边大，右边一定有peak，继续对右边二分
	4. 左边大右边小，左边一定有peak，继续对左边二分
需要注意的地方：
因为要将元素跟其左边和右边的元素比较，所以为了避免越界，初始需要将start设为1，end设为length-1；还需要把两个边界值设为负无穷，确保边界值也可以被选上。

class Solution:
    def findPeakElement(self, nums: List[int]) -> int:
        if len(nums) == 0:
            return -1
        #在左右分别插入1个负无穷，使得nums最边上的两个值也可以比较
        nums.insert(0, -float('inf'))
        nums.append(-float('inf'))
        #nums的有效位从1到倒数第二
        start, end = 1, len(nums) - 2
        
        #二分法三部曲
        while start + 1 < end:
            mid = start + (end - start) // 2
            """
            二分法选取mid元素，其跟左右元素的关系有如下四中情况：
                两边元素都比mid小，mid就是peak,，返回
                两边元素都比mid大，左右都有可能有peak，任选一边二分
                左边小又边大，右边一定有peak，继续对右边二分
                左边大右边小，左边一定有peak，继续对左边二分
            """
            if nums[mid-1] < nums[mid] > nums[mid+1]:
                return mid - 1
            if nums[mid-1] < nums[mid] < nums[mid+1]:
                start = mid
            if nums[mid-1] > nums[mid] > nums[mid+1]:
                end = mid
            if nums[mid+1] > nums[mid] < nums[mid-1]:
                start = mid
        if nums[start] < nums[end]:
            return end - 1 #记得最后的结果需要减一，因为之前插入了一个无穷
        else:
            return start - 1


暴力搜索
class Solution:
    def findPeakElement(self, nums: List[int]) -> int:
        return nums.index(max(nums))


二分
每次取中间元素，如果大于左右，则这就是peek。
否则取大的一边，两个都大，可以随便取一边。最终会找到peek。
class Solution:
    def findPeakElement(self, nums: List[int]) -> int:
        start = 0
        end = len(nums) - 1
        
        while start + 1 < end:
            mid = start + (end - start) // 2
            #三种情况
            if nums[mid] < nums[mid - 1]:
                end = mid
            elif nums[mid] < nums[mid + 1]:
                start = mid
            else:
                start = mid       
                
        if nums[start] > nums[end]:
            return start
        else:
            return end



这个题 LintCode 和 LeetCode 的 find peak element 是有区别的。
数据上，LintCode 保证数据第一个数比第二个数小，倒数第一个数比到倒数第二个数小。
因此 start, end 的范围要取 1, len(A) - 2
二分法。
每次取中间元素，如果大于左右，则这就是peek。
否则取大的一边，两个都大，可以随便取一边。最终会找到peek。
正确性证明：
	1. A[0] < A[1] && A[n-2] > A[n-1] 所以一定存在一个peek元素。
	2. 二分时，选择大的一边, 则留下的部分仍然满足1 的条件，即最两边的元素都小于相邻的元素。所以仍然必然存在peek。
二分至区间足够小，长度为3, 则中间元素就是peek。