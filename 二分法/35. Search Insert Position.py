Given a sorted array and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.
You may assume no duplicates in the array.
Example 1:
Input: [1,3,5,6], 5
Output: 2
Example 2:
Input: [1,3,5,6], 2
Output: 1
Example 3:
Input: [1,3,5,6], 7
Output: 4
Example 4:
Input: [1,3,5,6], 0
Output: 0

分析
二分法的问题一般都是找满足条件的firstposition和lastposition.
这道题是需要找到firstposition >= targrt，第一个>=target的位置。
while结束后，需要找firstposition的话先判断start，找lastposition的话先判断end。


class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        if len(nums) == 0:
            return -1
        start, end = 0, len(nums) - 1
        
        while start + 1 < end:
            mid = start + (end - start) // 2
            if nums[mid] == target:
                end = mid #找“第一个”大于target的位置，前就是后，第一就是end
            elif nums[mid] < target:
                start = mid
            else:
                end = mid
        #返回第一个大于target的位置
        if nums[start] >= target:
            return start
        elif nums[end] >= target:
            return end
        else:#如果都不大于target，证明target大于所有的数，即最后一位加1
            return end + 1
