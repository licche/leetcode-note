Description
中文
English
Given n pieces of wood with length L[i] (integer array). Cut them into small pieces to guarantee you could have equal or more than k pieces with the same length. What is the longest length you can get from the n pieces of wood? Given L & k, return the maximum length of the small pieces.
You couldn't cut wood into float length.
If you couldn't get >= k pieces, return 0.
Have you met this question in a real interview?  Yes
Problem Correction
Example
Example 1
Input:
L = [232, 124, 456]
k = 7
Output: 114
Explanation: We can cut it into 7 pieces if any piece is 114cm long, however we can't cut it into 7 pieces if any piece is 115cm long.
Example 2
Input:
L = [1, 2, 3]
k = 7
Output: 0
Explanation: It is obvious we can't make it.
Challenge
O(n log Len), where Len is the longest length of the wood.

来自 <https://www.lintcode.com/problem/wood-cut/description> 


class Solution:
    """
    @param L: Given n pieces of wood with length L[i]
    @param k: An integer
    @return: The maximum length of the small pieces
    """
    def woodCut(self, L, k):
        if not L:
            return 0
            
        #二分法分的是切的木头的长度，如果切的太短，数目会太大，切的太长数目会太小
        #切的数目必须大于等于k
        start = 1
        end = max(L)
        
        while start + 1 < end:
            mid = start + (end - start) // 2
            if self.count_pieces(mid, L) >= k:
                start = mid
            else:
                end = mid
        if self.count_pieces(end, L) >= k:
            return end
        if self.count_pieces(start, L) >= k:
            return start
        return 0
        
    def count_pieces(self, mid, L):
        sum = 0
        for wood in L:
            sum += wood // mid
            
        return sum

