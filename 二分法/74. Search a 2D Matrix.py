Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the following properties:
	• Integers in each row are sorted from left to right.
	• The first integer of each row is greater than the last integer of the previous row.
Example 1:
Input:
matrix = [
  [1,   3,  5,  7],
  [10, 11, 16, 20],
  [23, 30, 34, 50]
]
target = 3
Output: true
Example 2:
Input:
matrix = [
  [1,   3,  5,  7],
  [10, 11, 16, 20],
  [23, 30, 34, 50]
]
target = 13
Output: false
分析
两种做法：
	1. 先按每行的首个数字二分确定target所在的行，再在这行里二分。
	2. 看成一维数组，二分查找，其中看成一维数组的序号n和二维数组中行号、列号对应关系为：
r = n/columns;
c = n%columns;


class Solution:
    def searchMatrix(self, matrix: List[List[int]], target: int) -> bool:
        #行为空，列为空，返回F
        if len(matrix) == 0 or len(matrix[0]) == 0:
            return False
        
        #定义列值        
        col = len(matrix[0]) 

        #结束值是行乘列 - 1
        start, end = 0, len(matrix)*col - 1
        
        while start + 1 < end:
            mid = start + (end - start) // 2
            
            r = mid//col #值所在的二维位置，行是位置除以列
            c = mid%col #值所在的二维位置，列是位置取余列
            
            if matrix[r][c] == target:
                return True #找到即返回
            elif matrix[r][c] < target:
                start = mid
            else:
                end = mid
                
        if matrix[start//col][start%col] == target or matrix[end//col][end%col] == target:
            return True
        else:
            return False