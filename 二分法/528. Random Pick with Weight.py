Medium
6621844Add to ListShare
Given an array w of positive integers, where w[i] describes the weight of index i, write a function pickIndex which randomly picks an index in proportion to its weight.
Note:
	1. 1 <= w.length <= 10000
	2. 1 <= w[i] <= 10^5
	3. pickIndex will be called at most 10000 times.
Example 1:
Input: 
["Solution","pickIndex"]
[[[1]],[]]
Output: [null,0]
Example 2:
Input: 
["Solution","pickIndex","pickIndex","pickIndex","pickIndex","pickIndex"]
[[[1,3]],[],[],[],[],[]]
Output: [null,0,1,1,1,0]
Explanation of Input Syntax:
The input is two lists: the subroutines called and their arguments. Solution's constructor has one argument, the array w. pickIndex has no arguments. Arguments are always wrapped with a list, even if there aren't any.

来自 <https://leetcode.com/problems/random-pick-with-weight/>


class Solution:

    def __init__(self, w: List[int]):
        self.sum_list = []
        count = 0
        for i in range(len(w)):
            count += w[i]
            self.sum_list.append(count)

    def pickIndex(self) -> int:
        rand = random.randint(1, self.sum_list[-1])
        start = 0
        end = len(self.sum_list) - 1
        
        while start + 1 < end:
            mid = (start + end) // 2
            if self.sum_list[mid] < rand:
                start = mid
            else:
                end = mid

        if rand <= self.sum_list[start]:
            return start
        return end
