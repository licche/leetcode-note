Hard
5814874Add to ListShare
There are two sorted arrays nums1 and nums2 of size m and n respectively.
Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
You may assume nums1 and nums2 cannot be both empty.
Example 1:
nums1 = [1, 3]
nums2 = [2]
The median is 2.0
Example 2:
nums1 = [1, 2]
nums2 = [3, 4]
The median is (2 + 3)/2 = 2.5

来自 <https://leetcode.com/problems/median-of-two-sorted-arrays/> 


两个堆
import heapq

class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        if not nums1 and not nums2:
            return None
            
        self.min_heap = []
        self.max_heap = []
        
        m, n = len(nums1), len(nums2)
        
        #直接合并两个数组然后从头到尾放数字
        nums = nums1 + nums2
        
        for num in nums:
            self.push_heap(num)

        #如果只有1个数，返回最大堆的值
        if not self.min_heap:
            return -heapq.heappop(self.max_heap)
        
        #判断中位数有几个
        a = heapq.heappop(self.min_heap)
        b = -heapq.heappop(self.max_heap)
        
        if (m + n) % 2 == 0:
            return (a + b) / 2
        
        return b
    
    
    #两个堆
    def push_heap(self, num):
        if len(self.max_heap) <= len(self.min_heap):
            heapq.heappush(self.max_heap, -num)
        else:
            heapq.heappush(self.min_heap, num)
            
        if self.min_heap and self.min_heap[0] < -self.max_heap[0]:
            heapq.heappush(self.min_heap, -heapq.heappop(self.max_heap))
            heapq.heappush(self.max_heap, -heapq.heappop(self.min_heap))

