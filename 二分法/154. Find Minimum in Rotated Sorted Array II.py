Hard
545156FavoriteShare
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.
(i.e.,  [0,1,2,4,5,6,7] might become  [4,5,6,7,0,1,2]).
Find the minimum element.
The array may contain duplicates.
Example 1:
Input: [1,3,5]
Output: 1
Example 2:
Input: [2,2,2,0,1]
Output: 0
Note:
	• This is a follow up problem to Find Minimum in Rotated Sorted Array.
	• Would allow duplicates affect the run-time complexity? How and why?

分析
如果存在重复元素，无法使用二分查找使得时间复杂度为O(logn)，最坏时间复杂度只能是o(n)。
证明方法：黑盒测试
假设给定的数组中有一个1和n-1个2，此时mid=2，无法判断1在哪边

1.
class Solution:
    def findMin(self, nums: List[int]) -> int:
        minN = float('inf')
        for i in nums:
            if i < minN:
                minN = i
        return minN


2.
class Solution:
    def findMin(self, nums: List[int]) -> int:
        return min(nums)
