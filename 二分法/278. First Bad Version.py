You are a product manager and currently leading a team to develop a new product. Unfortunately, the latest version of your product fails the quality check. Since each version is developed based on the previous version, all the versions after a bad version are also bad.

Suppose you have n versions [1, 2, ..., n] and you want to find out the first bad one, which causes all the following ones to be bad.

You are given an API bool isBadVersion(version) which will return whether version is bad. Implement a function to find the first bad version. You should minimize the number of calls to the API.

Example:

Given n = 5, and version = 4 is the first bad version.

call isBadVersion(3) -> false
call isBadVersion(5) -> true
call isBadVersion(4) -> true

Then 4 is the first bad version. 


# The isBadVersion API is already defined for you.
# @param version, an integer
# @return a bool
# def isBadVersion(version):

class Solution:
    def firstBadVersion(self, n):
        """
        :type n: int
        :rtype: int
        """
        #如果就1个，start=end=1，没法判断
        if isBadVersion(1):
            return 1
        #从1开始，到n结束
        start, end = 1, n
        #找第一个start
        while start < end:
            mid = start + (end - start)//2
            #如果中间数是坏版本，那么就在前面找还有没坏版本，因为这很有可能不是第一个坏版本
            if isBadVersion(mid):
                end = mid
            else:#否则一定在这个之后，至少是mid+1
                start = mid +1
        return start


分析
有1到n个版本，找到第一个错误版本的位置。利用从第一个错误的开始之后后面的都是错的，用二分查找到第一个错误版本

# The isBadVersion API is already defined for you.
# @param version, an integer
# @return a bool
# def isBadVersion(version):

class Solution:
    def firstBadVersion(self, n):
        """
        :type n: int
        :rtype: int
        """
        #从1到n
        start, end = 1, n
        while start + 1 < end:
            mid = start + (end - start)//2
            if isBadVersion(mid):
                end = mid#如果是，就往前找，这个可能不是第一个
            else:
                start = mid#如果不是，就往后找，一定在后面
        if isBadVersion(start):
            return start
        elif isBadVersion(end):
            return end
        return -1
