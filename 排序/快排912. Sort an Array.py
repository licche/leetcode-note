Medium

Given an array of integers nums, sort the array in ascending order.
 
Example 1:
Input: nums = [5,2,3,1]
Output: [1,2,3,5]
Example 2:
Input: nums = [5,1,1,2,0,0]
Output: [0,0,1,1,2,5]

来自 <https://leetcode.com/problems/sort-an-array/> 







class Solution:
    def sortArray(self, nums: List[int]) -> List[int]:
        if not nums:
            return None
        
        self.quick_sort(nums, 0, len(nums) - 1)
        return nums
        
    def quick_sort(self, nums, start, end):
        if start >= end:
            return 
        
        #把开始位置和结束位置分别设置为指针left和right
        left, right = start, end
        
        #求中点的值，而不是下标，因为nums变了下标可能就变了，nums[mid]可能变化
        pivot = nums[(start + end) // 2]
        
        #从两头向中间遍历
        while left <= right :
            #找到中点前第一个大于中点值的位置
            while left <= right and nums[left] < pivot:
                left += 1
            #找中点后第一个小于中点值的位置
            while left <= right and pivot < nums[right]:
                right -= 1
            #如果能找到，交换他们的位置，并且更新指针
            if left <= right:
                nums[left], nums[right] = nums[right], nums[left]
                left += 1
                right -= 1
        
        #因为整个数组变成了[start-right-pivot-left-end]的结构，所以分别找中点两边的部分
        self.quick_sort(nums, start, right)
        self.quick_sort(nums, left, end)
