Medium

Find the kth largest element in an unsorted array. Note that it is the kth largest element in the sorted order, not the kth distinct element.
Example 1:
Input: [3,2,1,5,6,4] and k = 2
Output: 5
Example 2:
Input: [3,2,3,1,2,4,5,5,6] and k = 4
Output: 4
Note:
You may assume k is always valid, 1 ≤ k ≤ array's length.

来自 <https://leetcode.com/problems/kth-largest-element-in-an-array/> 

class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        if not nums:
            return None
        #记得是return
        return self.quick_select(nums, 0, len(nums) - 1, k)
    
    def quick_select(self, nums, start, end, k):
        #出口，如果开始等于结束，就返回这个数
        if start == end:
            return nums[start]
        
        #快排
        left, right = start, end
        pivot = nums[(start + end) // 2]
        
        while left <= right:
            while left <= right and nums[left] > pivot:
                left += 1
            while left <= right and nums[right] < pivot:
                right -= 1
            if left <= right:
                nums[left], nums[right] = nums[right], nums[left]
                left += 1
                right -= 1
        
        #如果k小于前面的个数，则在前面找
        if start + k - 1 <= right:
            return self.quick_select(nums, start, right, k)
        if start + k - 1 >= left:
            return self.quick_select(nums, left, end, k - (left - start))
        
        #如果既不在前面也不在后面，就是在中间
        return pivot
        
        
        
class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        heap = []
        for num in nums:
            heapq.heappush(heap, num)
            if len(heap) > k:
                heapq.heappop(heap)
            
        return heapq.heappop(heap)
        
   
   
class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        heapq.heapify(nums)
        return heapq.nlargest(k, nums)[-1]
