Description

Given an array of n objects with k different colors (numbered from 1 to k), sort them so that objects of the same color are adjacent, with the colors in the order 1, 2, ... k.
1. You are not suppose to use the library's sort function for this problem.
2. k <= n
Have you met this question in a real interview?  Yes
Problem Correction
Example
Example1
Input: 
[3,2,2,1,4] 
4
Output: 
[1,2,2,3,4]
Example2
Input: 
[2,1,1,2,2] 
2
Output: 
[1,1,2,2,2]
Challenge
A rather straight forward solution is a two-pass algorithm using counting sort. That will cost O(k) extra memory. Can you do it without using extra memory?

来自 <https://www.lintcode.com/problem/sort-colors-ii/description?_from=ladder&&fromId=1> 

使用分治法来解决。
传入两个区间，一个是颜色区间 color_from, color_to。另外一个是待排序的数组区间 index_from, index_to.
找到颜色区间的中点，将数组范围内进行 partition，<= color 的去左边，>color 的去右边。
然后继续递归。
时间复杂度 O(nlogk)O(nlogk) n是数的个数， k 是颜色数目。这是基于比较的算法的最优时间复杂度。
不基于比较的话，可以用计数排序（Counting Sort）

来自 <https://www.jiuzhang.com/solution/sort-colors-ii/#tag-highlight-lang-python> 

class Solution:
    """
    @param colors: A list of integer
    @param k: An integer
    @return: nothing
    """
    def sortColors2(self, colors, k):
        self.rainBowSort(colors, 1, k, 0, len(colors) - 1)
        
    
    def rainBowSort(self, colors, color_from, color_to, index_start, index_end):
        if color_from == color_to or index_start == index_end:
        		return
        
        color_pivot = (color_from + color_to) // 2
        
				left, right = index_start, index_end
        
				while left <= right:
						while left <= right and nums[left] > pivot:
								left += 1

						while left <= right and nums[right] < pivot:
								right -= 1

						if left <= right:
								nums[left], nums[right] = nums[right], nums[left]
								left += 1
								right -= 1

       
        self.rainBowSort(colors, color_from, color_pivot, index_start, right)
        self.rainBowSort(colors, color_pivot + 1, color_to, left, index_end)
