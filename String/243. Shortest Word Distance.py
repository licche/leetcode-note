Easy
38835Add to ListShare
Given a list of words and two words word1 and word2, return the shortest distance between these two words in the list.
Example:
Assume that words = ["practice", "makes", "perfect", "coding", "makes"].
Input: word1 = “coding”, word2 = “practice”
Output: 3
Input: word1 = "makes", word2 = "coding"
Output: 1
Note:
You may assume that word1 does not equal to word2, and word1 and word2 are both in the list.
Accepted

来自 <https://leetcode.com/problems/shortest-word-distance/> 

class Solution:
    def shortestDistance(self, words: List[str], word1: str, word2: str) -> int:
        distance = float('inf')
        
        #p1 p2分别记录word1和word2的位置
        p1 = p2 = float('inf')
        for i in range(len(words)):
            if words[i] == word1:
                p1 = i
            if words[i] == word2:
                p2 = i
                
            #求出距离的最小值
            distance = min(distance, abs(p1 - p2))
        
        
        return distance         
