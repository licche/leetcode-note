Medium
295101Add to ListShare
Design a class which receives a list of words in the constructor, and implements a method that takes two words word1 and word2 and return the shortest distance between these two words in the list. Your method will be called repeatedly many times with different parameters. 
Example:
Assume that words = ["practice", "makes", "perfect", "coding", "makes"].
Input: word1 = “coding”, word2 = “practice”
Output: 3
Input: word1 = "makes", word2 = "coding"
Output: 1
Note:
You may assume that word1 does not equal to word2, and word1 and word2 are both in the list.

来自 <https://leetcode.com/problems/shortest-word-distance-ii/> 

class WordDistance:

    def __init__(self, words: List[str]):
        #哈希表存每个单词对应的index
        self.hash_set = collections.defaultdict(list)
        
        for i in range(len(words)):
            self.hash_set[words[i]].append(i)
            
                
        

    def shortest(self, word1: str, word2: str) -> int:

        short = float('inf')
        
        #找到差值最小的两个点
        for i in self.hash_set[word1]:
            for j in self.hash_set[word2]:
                short = min(short, abs(i - j))
        """
        i = j = 0
        while i < len(self.hash_set[word1]) and j < len(self.hash_set[word2]):
            p1 = self.hash_set[word1][i]
            p2 = self.hash_set[word2][j]
            
            if p2 > p1:
                short = min(short, p2 - p1)
                i += 1
            else:
                short = min(short, p1 - p2)
                j += 1
        """
        
                
        return short

