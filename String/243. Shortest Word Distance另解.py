Easy
31931FavoriteShare
Given a list of words and two words word1 and word2, return the shortest distance between these two words in the list.
Example:
Assume that words = ["practice", "makes", "perfect", "coding", "makes"].
Input: word1 = “coding”, word2 = “practice”
Output: 3
Input: word1 = "makes", word2 = "coding"
Output: 1
Note:
You may assume that word1 does not equal to word2, and word1 and word2 are both in the list.

来自 <https://leetcode.com/problems/shortest-word-distance/> 

class Solution:
    def shortestDistance(self, words: List[str], word1: str, word2: str) -> int:
        #初始化两个下标为-1，初始化最小距离为无穷大
        l1 = l2 = -1
        length = len(words)
        minLength = float('inf')
        #遍历列表，记录位置
        for i in range(length):
            if word1 == words[i]:
                l1 = i
            if word2 == words[i]:
                l2 = i
            #如果l1和l2都找到了，再比较大小，挑距离近的
            if l1 != -1 and l2 != -1:
                minLength = min(minLength, abs(l2-l1))
        return minLength

