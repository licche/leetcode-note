Medium
17265Add to ListShare
Given a list of words and two words word1 and word2, return the shortest distance between these two words in the list.
word1 and word2 may be the same and they represent two individual words in the list.
Example:
Assume that words = ["practice", "makes", "perfect", "coding", "makes"].
Input: word1 = “makes”, word2 = “coding”
Output: 1
Input: word1 = "makes", word2 = "makes"
Output: 3
Note:
You may assume word1 and word2 are both in the list.

来自 <https://leetcode.com/problems/shortest-word-distance-iii/> 

class Solution:
    def shortestWordDistance(self, words: List[str], word1: str, word2: str) -> int:
        hash = collections.defaultdict(list)
        
        #每个单词的index存入哈希表
        for i in range(len(words)):
            hash[words[i]].append(i)
        
        short = float('inf')
        
        #如果单词相同，就取word1的list，找两个index最近的值
        if word1 == word2:
            list_word = sorted(hash[word1])
            for i in range(1, len(list_word)):
                short = min(short, abs(list_word[i] - list_word[i - 1]))
        
        #如果不同就分别在两个哈希表记录的两个list里找距离最近的点
        else:
            for i in hash[word1]:
                for j in hash[word2]:
                    short = min(short, abs(j - i))
                    
        
                
                
        return short