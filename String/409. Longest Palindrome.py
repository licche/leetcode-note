Easy
63966FavoriteShare
Given a string which consists of lowercase or uppercase letters, find the length of the longest palindromes that can be built with those letters.
This is case sensitive, for example "Aa" is not considered a palindrome here.
Note:
Assume the length of given string will not exceed 1,010.
Example:
Input:
"abccccdd"
Output:
7
Explanation:
One longest palindrome that can be built is "dccaccd", whose length is 7.

来自 <https://leetcode.com/problems/longest-palindrome/> 

class Solution(object):
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: int
        """
        #造哈希表
        dict = {}
        
        for i in s:
            #i在表里，就删除它（这样可以保证是一对）
            if i in dict:
                del dict[i]
            #不在就设置为True
            else:
                dict[i] = True
        #如果多出来至少1个，可以取其中一个作为回文最中间的
        remove = len(dict)
        if remove > 0:
            remove -= 1
        #返回总长减去其他
        return len(s) - remove